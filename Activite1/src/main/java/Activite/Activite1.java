package Activite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
/*
 * @auth Ainas Nouria
 */
public class Activite1 {

    public static void main(String[] args) {
        // Exemple d'utilisation
        long startTime = System.currentTimeMillis();

        TreeMap<String, Integer> termIdTreeMap = loadTerms("/home/dptinfo/Documents/IAGL/teste.txt");

        long endTime = System.currentTimeMillis();
        long loadingTime = endTime - startTime;
        System.out.println("Temps de chargement : " + loadingTime + " millisecondes");

        // Exemple d'utilisation
        startTime = System.currentTimeMillis();

        String searchTerm = "Mari"; // Terme entré par l'utilisateur
        List<Pair<String, Integer>> matchingTerms = findMatchingTerms(termIdTreeMap, searchTerm);

        if (matchingTerms != null && !matchingTerms.isEmpty()) {
            for (Pair<String, Integer> pair : matchingTerms) {
                System.out.println("Mot composé : " + pair.getFirst() + ", ID : " + pair.getSecond());
            }
        } else {
            System.out.println("Aucun mot composé trouvé pour le préfixe : " + searchTerm);
        }

        endTime = System.currentTimeMillis();
        long searchTime = endTime - startTime;
        System.out.println("Temps de recherche : " + searchTime + " millisecondes");

        // Obtenir l'utilisation mémoire approximative
        Runtime runtime = Runtime.getRuntime();
        long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Empreinte en RAM : " + usedMemory / (1024 * 1024) + " Mo");
    }

    private static TreeMap<String, Integer> loadTerms(String filePath) {
        TreeMap<String, Integer> termIdTreeMap = new TreeMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            long startTime = System.currentTimeMillis();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(";");
                if (parts.length == 2) {
                    String currentTerm = parts[1].replaceAll("\"", "");
                    int id = Integer.parseInt(parts[0]);
                    termIdTreeMap.put(currentTerm, id);
                }
            }

            long endTime = System.currentTimeMillis();
            long loadingTime = endTime - startTime;
            System.out.println("Temps de chargement : " + loadingTime + " millisecondes");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return termIdTreeMap;
    }

    private static List<Pair<String, Integer>> findMatchingTerms(TreeMap<String, Integer> termIdTreeMap, String prefix) {
        List<Pair<String, Integer>> matchingTerms = new ArrayList<>();

        for (String term : termIdTreeMap.keySet()) {
            if (term.startsWith(prefix)) {
                int id = termIdTreeMap.get(term);
                matchingTerms.add(new Pair<>(term, id));
            }
        }

        return (matchingTerms.isEmpty()) ? null : matchingTerms;
    }

    // Classe Pair pour stocker deux éléments
    static class Pair<T, U> {
        private final T first;
        private final U second;

        public Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }

        public T getFirst() {
            return first;
        }

        public U getSecond() {
            return second;
        }
    }
}
