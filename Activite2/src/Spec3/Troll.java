package Spec3;
/*
 * @auth Ainas Nouria
 */
public class Troll extends Personnage implements Monstre {
    private int force;
    private int puanteur;

    public Troll(String nom, int x, int y, int v, int force, int puanteur) {
        super(nom, x, y, v);
        this.force = force;
        this.puanteur = puanteur;
    }

    @Override
    public void attaque(Personnage p) {
        if (!(p instanceof Troll)) {
            int degats = force + puanteur / 10;
            p.recevoirDegats(degats);
        }
    }

	@Override
	public String parler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void recevoirDegats(int degats) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPuanteur() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected int getPrecision() {
		// TODO Auto-generated method stub
		return 0;
	}
}
