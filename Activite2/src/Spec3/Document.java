package Spec3;
/*
 * @auth Ainas Nouria
 */
public class Document extends Objet {
    private int connaissances;
    private boolean lu;
    private Personnage proprietaire;

    public Document(String nom, int prix, int connaissances, Personnage proprietaire) {
        super(nom, prix);
        this.connaissances = connaissances;
        this.lu = false;
        this.proprietaire = proprietaire;
    }

    public void lire() {
        if (!lu) {
            proprietaire.setConnaissances(proprietaire.getConnaissances() + connaissances);
            lu = true;
        }
    }

    public void setProprietaire(Personnage proprietaire) {
        this.proprietaire = proprietaire;
    }
}
