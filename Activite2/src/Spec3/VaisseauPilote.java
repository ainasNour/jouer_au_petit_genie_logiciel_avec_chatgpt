package Spec3;
/*
 * @auth Ainas Nouria
 */
import java.util.Random;

public class VaisseauPilote extends VaisseauSpatial {
    private Personnage pilote;

    public VaisseauPilote(int etat, int blindage, Personnage pilote) {
        super(etat, blindage);
        this.pilote = pilote;
    }

    @Override
    public void recevoirTir(Arme arme) {
        // Utiliser la précision du pilote pour déterminer si le tir touche
        Random random = new Random();
        if (random.nextInt(100) < pilote.getPrecision()) {
            super.recevoirTir(arme); // Appel à la méthode de la classe parente
        }
    }
}