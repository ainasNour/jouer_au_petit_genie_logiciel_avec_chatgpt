package Spec3;
/*
 * @auth Ainas Nouria
 */
public class VaisseauSpatial implements Cible {
    private int etat;
    private int blindage;

    public VaisseauSpatial(int etat, int blindage) {
        this.etat = etat;
        this.blindage = blindage;
    }

    @Override
    public void recevoirTir(Arme arme) {
        int degats = arme.getPuissance() / blindage;
        etat -= degats;
        if (etat < 0) {
            etat = 0; // Le vaisseau est détruit
        }
    }
    public int getEtat() {
        return etat;
    }
}
