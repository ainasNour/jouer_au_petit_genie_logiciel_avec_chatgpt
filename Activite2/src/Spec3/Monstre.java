package Spec3;
/*
 * @auth Ainas Nouria
 */
public interface Monstre {
    void attaque(Personnage p);

    int getPuanteur();
}