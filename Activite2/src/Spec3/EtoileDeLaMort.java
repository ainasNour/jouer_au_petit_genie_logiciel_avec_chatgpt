package Spec3;

import Spec1.VaisseauPilote;
/*
 * @auth Ainas Nouria
 */
public class EtoileDeLaMort extends VaisseauPilote {
    private static final int PUISSANCE = 1000;
    private static final int BLINDAGE = 5;

    private static EtoileDeLaMort uniqueInstance;

    private EtoileDeLaMort(Spec1.Personnage pilote) {
        super(100, BLINDAGE, pilote);
    }
    private EtoileDeLaMort(Humanoide pilote) {
        super(100, BLINDAGE, pilote);
    }
    public static EtoileDeLaMort getInstance1(Humanoide pilote) {
        if (uniqueInstance == null) {
            uniqueInstance = new EtoileDeLaMort(pilote);
        }
        return uniqueInstance;
    }


    public static EtoileDeLaMort getInstance(Spec1.Personnage pilote) {
        if (uniqueInstance == null) {
            uniqueInstance = new EtoileDeLaMort(pilote);
        }
        return uniqueInstance;
    }

	public void parler() {
	System.out.println("Pew pew");
		
	}
}
