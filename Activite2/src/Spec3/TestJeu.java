package Spec3;
/*
 * @auth Ainas Nouria
 */
public class TestJeu {

    public static void main(String[] args) {
        // Création d'un humanoïde
        Humanoide aragorn = new Humanoide("Aragorn", 10, 20, 5);

        // Création d'une arme
        Arme epee = new Arme("Anduril", 100, 20);

        // Création d'un document
        Document carte = new Document("Carte de la Terre du Milieu", 50, 30, aragorn);

        // Création d'un troll
        Troll troll = new Troll("Gorbag", 30, 40, 3, 15, 5);

        // Aragorn prend l'épée et lit la carte
        aragorn.prendreObjet(epee);
        aragorn.prendreObjet(carte);
        aragorn.parler(); // Affiche : "Je parle donc je suis."

        // Aragorn attaque le troll
        aragorn.attaque(troll);
        System.out.println("Points de vie du troll : " + troll.getPointsVie()); // Affiche : "Points de vie du troll : 85"

        // Le troll attaque Aragorn
        troll.attaque(aragorn);
        System.out.println("Points de vie d'Aragorn : " + aragorn.getPointsVie()); // Affiche : "Points de vie d'Aragorn : 80"

        // Aragorn lit à nouveau la carte (mais cela n'augmente pas ses connaissances une deuxième fois)
        carte.lire();
        System.out.println("Connaissances d'Aragorn : " + aragorn.getConnaissances()); // Affiche : "Connaissances d'Aragorn : 30"

        // Création de l'Étoile de la Mort
        EtoileDeLaMort etoileDeLaMort = EtoileDeLaMort.getInstance1(aragorn);
        etoileDeLaMort.parler(); // Affiche : "Pew pew!"

        // Création d'un vaisseau spatial
        VaisseauSpatial xwing = new VaisseauSpatial(50, 60);

        // Tir de l'Étoile de la Mort sur le vaisseau spatial
        etoileDeLaMort.recevoirTir(epee);
        System.out.println("État du vaisseau spatial X-Wing : " + xwing.getEtat()); // Affiche : "État du vaisseau spatial X-Wing : 70"
    }
}
