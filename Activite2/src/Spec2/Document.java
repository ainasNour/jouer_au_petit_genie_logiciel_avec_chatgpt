package Spec2;
/*
 * @auth Ainas Nouria
 */
public class Document extends Objet {
    private int quantiteConnaissances;
    private boolean lu;

    public Document(String nom, int prix, int quantiteConnaissances) {
        super(nom, prix);
        this.quantiteConnaissances = quantiteConnaissances;
        this.lu = false;
    }

    public int getQuantiteConnaissances() {
        if (!lu) {
            lu = true;
            return quantiteConnaissances;
        }
        return 0;
    }
}
