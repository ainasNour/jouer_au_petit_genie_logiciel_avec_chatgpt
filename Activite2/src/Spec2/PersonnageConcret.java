package Spec2;
/*
 * @auth Ainas Nouria
 */
public class PersonnageConcret extends Personnage {

    public PersonnageConcret(String nom, int x, int y, int v) {
        super(nom, x, y, v);
    }

    @Override
    public void attaque(Personnage p) {
        if (!(p instanceof Troll)) {
            // Logique d'attaque pour un personnage concret
            // Par exemple, infliger des dégâts à la cible
            int degats = 10; // Vous pouvez ajuster la valeur selon votre logique
            p.recevoirDegats(degats);
        }
    }

    @Override
    public void recevoirDegats(int degats) {
        // Implémentation de la réception des dégâts
        pointsVie -= degats;
        if (pointsVie < 0) {
            pointsVie = 0;
        }
    }

    @Override
    public void seDeplacer(int dx, int dy, int t) {
       
    	this.x = (int) (this.x + dx*this.v*t/Math.sqrt(dx*dx+dy*dy));
    	this.y = (int) (this.y + dy*this.v*t/Math.sqrt(dx*dx+dy*dy));
    }

    @Override
    public String parler() {
        return "Bonjour, je suis un personnage concret !";
    }
}
