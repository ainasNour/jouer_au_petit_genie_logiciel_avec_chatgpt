package Spec2;
/*
 * @auth Ainas Nouria
 */
import java.util.ArrayList;
import java.util.List;

public class Humanoide extends Personnage {
    private List<Objet> inventaire;

    public Humanoide(String nom, int x, int y, int v) {
        super(nom, x, y, v);
        this.inventaire = new ArrayList<>();
    }

    public void acquérirObjet(Objet objet) {
        inventaire.add(objet);
    }

    public void seSéparerObjet(Objet objet) {
        inventaire.remove(objet);
    }

    public void donnerObjet(Humanoide destinataire, Objet objet) {
        if (inventaire.contains(objet)) {
            destinataire.acquérirObjet(objet);
            seSéparerObjet(objet);
        }
    }

	@Override
	public String parler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void attaque(Personnage p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recevoirDegats(int degats) {
		// TODO Auto-generated method stub
		
	}
}