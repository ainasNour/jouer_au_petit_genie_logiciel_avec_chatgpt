package Spec2;
/*
 * @auth Ainas Nouria
 */
public class JeuSeigneurDesAnneaux {
    public static void main(String[] args) {
        // Création d'un personnage concret
        PersonnageConcret heros = new PersonnageConcret("Aragorn", 0, 0, 5);

        // Création d'un troll
        Troll troll = new Troll("Troll1", 0, 0, 5, 10, 0);

        // Le héros attaque le troll
        System.out.println("Points de vie du troll avant l'attaque : " + troll.getPointsVie());
        heros.attaque(troll);
        System.out.println("Points de vie du troll après l'attaque : " + troll.getPointsVie());

        // Le troll attaque le héros
        System.out.println("Points de vie du héros avant l'attaque : " + heros.getPointsVie());
        troll.attaque(heros);
        System.out.println("Points de vie du héros après l'attaque : " + heros.getPointsVie());
    }
}

