package Spec1;
/*
 * @auth Ainas Nouria
 */
public class PersonnageConcret extends Personnage {
    // Constructeur
    public PersonnageConcret(String n, int x, int y, int v, int precision) {
        super(n, x, y, v, precision);
    }

    // Implémentation des méthodes abstraites de la classe Personnage
    @Override
    public void recevoirDegats(int degats) {
        // Implémentation de la réception des dégâts
        pointsVie -= degats;
        if (pointsVie < 0) {
            pointsVie = 0;
        }
    }

    @Override
    public void augmenterConnaissances(int quantite) {
        // Implémentation de l'augmentation des connaissances
        // Ajoutez les connaissances reçues
        // Vous pouvez ajuster cette implémentation en fonction de vos besoins spécifiques
        int nouvellesConnaissances = getConnaissances() + quantite;
        setConnaissances(nouvellesConnaissances);
    }

    

	@Override
    public String parler() {
        return "Bonjour, je suis un personnage concret !";
    }
}
