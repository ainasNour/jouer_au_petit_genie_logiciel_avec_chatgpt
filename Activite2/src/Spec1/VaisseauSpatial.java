package Spec1;
/*
 * @auth Ainas Nouria
 */
public class VaisseauSpatial implements Cible {
    private int etat;
    private int blindage;

    public VaisseauSpatial(int etat, int blindage) {
        this.etat = etat;
        this.blindage = blindage;
    }

    public int getEtat() {
        return etat;
    }

	@Override
	public void recevoirTir(Spec3.Arme epee) {
		  int degats = epee.getPuissance() / blindage;
	        etat -= degats;
	        if (etat < 0) {
	            etat = 0; // Le vaisseau est détruit
	        }
	}

	@Override
	public void recevoirTir(Arme epee) {
		// TODO Auto-generated method stub
		
	}
}
