package Spec1;
/*
 * @auth Ainas Nouria
 */

public class EtoileDeLaMort extends VaisseauPilote {
    private static final int PUISSANCE = 1000;
    private static final int BLINDAGE = 5;

    private static EtoileDeLaMort uniqueInstance;

    private EtoileDeLaMort(Personnage pilote) {
        super(100, BLINDAGE, pilote);
    }

    public static EtoileDeLaMort getInstance(Personnage pilote) {
        if (uniqueInstance == null) {
            uniqueInstance = new EtoileDeLaMort(pilote);
        }
        return uniqueInstance;
    }
}
