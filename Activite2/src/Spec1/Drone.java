package Spec1;

import java.util.Random;
/*
 * @auth Ainas Nouria
 */
public class Drone extends VaisseauSpatial {
    private int precisionIntrinseque;

    public Drone(int etat, int blindage, int precisionIntrinseque) {
        super(etat, blindage);
        this.precisionIntrinseque = precisionIntrinseque;
    }

    @Override
    public void recevoirTir(Arme arme) {
        // Utiliser la précision intrinsèque du drone pour déterminer si le tir touche
        Random random = new Random();
        if (random.nextInt(100) < precisionIntrinseque) {
            super.recevoirTir(arme); // Appel à la méthode de la classe parente
        }
    }
}