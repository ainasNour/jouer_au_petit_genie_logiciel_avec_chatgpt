package Spec1;
/*
 * @auth Ainas Nouria
 */
public class JeuStarWars {
    public static void main(String[] args) {
        // Création d'une arme
        Arme arme = new Arme(50);

        // Création d'un vaisseau spatial
        VaisseauSpatial vaisseauSpatial = new VaisseauSpatial(100, 3);

        // Création d'un personnage
        PersonnageConcret pilote = new PersonnageConcret("Luke Skywalker", 0, 0, 5, 80);

        // Création d'un vaisseau pilote avec un pilote
        VaisseauPilote vaisseauPilote = new VaisseauPilote(100, 3, pilote);

        // Création d'un drone
        Drone drone = new Drone(100, 2, 70);

        // Création de l'Étoile de la Mort avec un pilote
        EtoileDeLaMort etoileDeLaMort = EtoileDeLaMort.getInstance(pilote);

        // Test de tir sur les cibles
        System.out.println("Vaisseau Spatial avant tir : État = " + vaisseauSpatial.getEtat());
        arme.tirer(vaisseauSpatial);
        System.out.println("Vaisseau Spatial après tir : État = " + vaisseauSpatial.getEtat());

        System.out.println("Vaisseau Pilote avant tir : État = " + vaisseauPilote.getEtat());
        arme.tirer(vaisseauPilote);
        System.out.println("Vaisseau Pilote après tir : État = " + vaisseauPilote.getEtat());

        System.out.println("Drone avant tir : État = " + drone.getEtat());
        arme.tirer(drone);
        System.out.println("Drone après tir : État = " + drone.getEtat());

        System.out.println("Étoile de la Mort avant tir : État = " + etoileDeLaMort.getEtat());
        arme.tirer(etoileDeLaMort);
        System.out.println("Étoile de la Mort après tir : État = " + etoileDeLaMort.getEtat());
    }
}
