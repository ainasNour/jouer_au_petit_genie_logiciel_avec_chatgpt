package Spec1;
/*
 * @auth Ainas Nouria
 */
public abstract class Personnage {
    protected String nom;
    protected int pointsVie, x, y, v, precision; // precision de 0 à 100
    protected int connaissances; // Ajout de l'attribut pour les connaissances

    public Personnage(String n, int x, int y, int v, int precision) {
        this.nom = n;
        this.x = x;
        this.y = y;
        this.pointsVie = 100;
        this.v = v;
        this.precision = precision;
        this.connaissances = 0; // Initialisation des connaissances à 0
    }

    public int getPointsVie() {
        return this.pointsVie;
    }

    public void setPointsVie(int pv) {
        this.pointsVie = pv;
    }

    public int getPrecision() {
        return this.precision;
    }

    public int getConnaissances() { // Ajout de la méthode pour obtenir les connaissances
        return this.connaissances;
    }
    public int setConnaissances(int con) {
    	return this.connaissances;
    }

    public void seDeplacer(int dx, int dy, int t) {
        this.x = (int) (this.x + dx * this.v * t / Math.sqrt(dx * dx + dy * dy));
        this.y = (int) (this.y + dy * this.v * t / Math.sqrt(dx * dx + dy * dy));
    }

    // Méthode abstraite pour recevoir des dégâts
    public abstract void recevoirDegats(int degats);

    // Méthode abstraite pour augmenter les connaissances
    public abstract void augmenterConnaissances(int quantite);

    // Méthode abstraite pour parler
    public abstract String parler();
}
