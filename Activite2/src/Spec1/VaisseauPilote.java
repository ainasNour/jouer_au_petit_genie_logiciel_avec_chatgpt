package Spec1;
/*
 * @auth Ainas Nouria
 */
import java.util.Random;

import Spec3.Humanoide;

public class VaisseauPilote extends VaisseauSpatial {
    private Personnage pilote;
    private Humanoide pilote2;

    public VaisseauPilote(int etat, int blindage, Personnage pilote) {
        super(etat, blindage);
        this.pilote = pilote;
    }

    public VaisseauPilote(int etat, int blindage, Humanoide pilote2) {
    	  super(etat, blindage);
          this.pilote2 = pilote2;
		
	}

	
}