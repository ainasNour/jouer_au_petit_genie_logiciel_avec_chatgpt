package Activite3;

public class Main {
	/*
	 * @auth Ainas Nouria
	 */
	public static void main(String[] args) {
        AVLTree avlTree = new AVLTree();

        // Test d'insertion
        avlTree.racine = avlTree.inserer(avlTree.racine, 10);
        avlTree.racine = avlTree.inserer(avlTree.racine, 20);
        avlTree.racine = avlTree.inserer(avlTree.racine, 30);
        avlTree.racine = avlTree.inserer(avlTree.racine, 15);
        avlTree.racine = avlTree.inserer(avlTree.racine, 5);

        // Affichage de l'arbre
        avlTree.afficherArbre(avlTree.racine, 0);

        // Test de suppression
        avlTree.racine = avlTree.supprimer(avlTree.racine, 20);

        // Affichage de l'arbre après suppression
        avlTree.afficherArbre(avlTree.racine, 0);
    }}
